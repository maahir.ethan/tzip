---
title: Transfer and call entrypoint for Financial Assets
status: Draft
author: SmartPy
type: Financial Application
created: 2021-05-12
date: 2021-05-12
version: 1
---

# Transfer and call entrypoint for Financial Assets

## Summary

TZIP-23 extends FA2 token specifications by introducing a multi-action transfer
entry point. The entry point adds a proxy mechanism to the standard transfer
behavior, allowing the source to send a request or notify some contract
after a successful transfer.

## Abstract

This TZIP adds a new entry point named `transfer_and_call` to the FA2
specification. The entry point can transfer tokens to a given recipient and send
a follow-up call to a contract. The follow-up call is a callback provided by the
sender to proxy some data of type bytes to a given destination address.

The `transfer_and_call` is inspired by [ERC677][ERC677]).

[ERC677]: https://github.com/ethereum/EIPs/issues/677

## Motivation

Currently, the FA2 transfer standard doesn't offer any mechanism to allow the
sender to notify the recipient of a transaction. This mechanism is necessary to
improve the interoperability between contracts. As a result, it will turn the
communication more decentralized by removing off-chain dependencies.

## General

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be
interpreted as described in [RFC 2119][RFC 2119].

[RFC 2119]: https://www.ietf.org/rfc/rfc2119.txt

## Interface Specification

Token contract implementing this TZIP standard MUST implement the given
entrypoint: [`transfer_and_call`](#transfer_and_call)

### `transfer_and_call`

``` michelson
(list %transfer_and_call
  (pair
    (address %from_)
    (list %txs
      (pair
        (nat %amount)
        (bytes %data)
        (address %callback)
        (address %to_)
        (nat %token_id)
      )
    )
  )
)
```

The entry point SHALL perform the following two actions:

1. Regular transfer
2. Callback

The entrypoint SHOULD NOT perform any extra logic and SHOULD NOT call any other
contract or rely on any other instruction that can fail because of a third
party.

### Regular transfer

The transfer part of the `transfer_and_call` MUST work **exactly** like a call
to the `transfer` entrypoint. (see [FA2#transfer][FA2#transfer]).

[FA2#transfer]: ../tzip-12/tzip-12.md#transfer

### Callback

The contract MUST return a list of transfer operation for each `%callback` in
the **same order** that the transfer are performed without deduplicating or
aggregating them with the following type argument:

```
(pair
  (nat %amount)
  (bytes %data)
  (address %receiver)
  (address %sender)
  (nat %token_id)
)
```

The transaction MUST FAILWITH `"FA2_WRONG_CALLBACK_INTERFACE"` if a
contract of the right type cannot be built from the callback address.

## Errors

| Error mnemonic                               | Description                                                               |
|----------------------------------------------|---------------------------------------------------------------------------|
| `"FA2_WRONG_CALLBACK_INTERFACE"`             | The `%callback` cannot be transformed to a contract of the callback type. |

## Rationale

Some alternative interface design have been rejected.

### Rejected Alternative 1: without `from_` param

``` michelson
(list %transfer_and_call
  (pair
    (address %to_)
    (address %callback)
    (bytes %data)
    (list
      (pair
        (nat %token_id)
        (nat %amount)
      )
    )
  )
)
```
This design was propose to simplify the entrypoint by removing operators.
The `data` parameter can represent a complex order that differs largely from
what an operator was allowed to do before `transfer_and_call`. Also, the FA2
operators description were not covering this kind of authorization.

This design is rejected because the `from_` feature is very usefull and the
`transfer_and_call` should be as close as `transfer` as possible.

### Rejected Alternative 2: without `receiver` in the callback entrypoint.

The previous version of this proposal did not contained the `receiver` param.
As a consequence the FA2 contract was responsible for verifying that `receiver`
and `callback` were representing the same underlying address.

Michelson is lacking an official solution of how to do this and the proposed
solution would not be guaranteed to work in the future. Furthermore, being able
to specify a different underlying address can be useful in certain cases.

As a consequence, the FA2 contract is simplified and the receiving contract is
now a bit more complex than in the previous version.

## Security Considerations

### Receiving verification

A contract that implements the receiving of a transfer_and_call IS RESPONSIBLE
for verifying that the `receiver` corresponds to its own address or one it
reacts to.

### Callback failure

Contrary to the entry points currently defined in the FA2 specification,
`transfer_and_call` creates internal calls to the recipient contracts. **The
whole transaction can fail if one of the recipient calls fails.** This can lead
to anti-patterns when the sender must perform other logic in the same
transaction. The entrypoint must be considered like a classic fallible call.

Example: A smartcontract send a batch with transfers and call to two different
address. One of the receiver fails on purpose (or not). The other receipient
can't receive his tokens.

## Implementations

+ [SmartPy](examples/fa2_transfer_and_call.py)

## Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
